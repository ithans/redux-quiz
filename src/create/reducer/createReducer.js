const initState = {
  newNOte: false
};
export default (state = initState, action) => {
  switch (action.type) {
    case 'CREATE_NOTE':
      return {
        ...state,
        newNOte: action.newNOte
      };
    default:
      return state
  }
};