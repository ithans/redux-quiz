import React from 'react'
import { createNote } from "../actions/createAction";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

class Create extends React.Component {
  componentDidMount() {
    this.props.createNote();
  }

  createNote(even){
    const title = document.getElementById('title').value;
    const description = document.getElementById('description').value;
    this.props.createNote(title, description);
    window.location.href = '/';
  }
  cancelNote(even){
    window.location.href = '/';
  }
  render() {
    return (
      <div>
        <h1>创建笔记</h1>
        <hr/>
        <label>标题</label>
        <br/>
        <input type="text" name="title" id="title"/>
        <br/>
        <label>正文</label>
        <br/>
        <textarea name="description" id="description"></textarea>
        <button onClick={this.createNote.bind(this)}>提交</button>
        <button onClick={this.cancelNote.bind(this)}>取消</button>
      </div>)
  }
}
const mapStateToProps = state => ({
  newNote: state.creater.newNote
});

const mapDispatchToProps = dispatch => bindActionCreators({
  createNote
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Create);
