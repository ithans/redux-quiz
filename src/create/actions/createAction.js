export const createNote = (title,description) => (dispatch) => {
  fetch('http://localhost:8080/api/posts',{
    method:"POST",
    body:JSON.stringify({title: title, description: description}),
    headers:{"Content-Type":"application/json;charset=UTF-8"}
  })
    .then(response =>response.json())
    .then(result => {
      dispatch({
        type:'CREATE_NOTE',
        newNote:result
      })
    })
};