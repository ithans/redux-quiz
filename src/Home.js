import React from 'react'
import { getAllNotes } from "./home/actions/homeAction";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

class Home extends React.Component {

  componentDidMount() {
    this.props.getAllNotes();
  }

  render() {
    const result = this.props.markDowns;
    return (
     <div>
       {result.map((value,index) => {
         return (
           <div className='list'>
             <Link key={'div' + index} to={'/notes/' + value.id}>
              {value.title}
             </Link>
           </div>
         )
       })}
       <div className='list'>
         <Link to={'/notes/create'}>+</Link>
       </div>

     </div>
    )
  }
}

const mapStateToProps = state => ({
  markDowns: state.notes.markDowns
});

const mapDispatchToProps = dispatch => bindActionCreators({
  getAllNotes
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Home);
