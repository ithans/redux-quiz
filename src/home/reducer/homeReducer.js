const initState = {
  markDowns: []
};

export default (state = initState, action) => {
  switch (action.type) {
    case 'GET_ALL_NOTES':
      return {
        ...state,
        markDowns: action.markDowns
      };

    default:
      return state
  }
};