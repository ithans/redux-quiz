import React, {Component} from 'react';
import './App.less';
import { BrowserRouter as Router } from "react-router-dom";
import { Route, Switch } from "react-router";
import Home from "./Home";
import Note from "./note/page/Note";
import Create from "./create/page/Create";

class App extends Component{
  render() {
      return(
        <div className='App'>
          <Router>
            <Switch>
              <Route path={'/notes/create'} component={Create}/>
              <Route path={'/notes/:id'} component={Note}/>
              <Route path={"/"} component={Home}/>
            </Switch>
          </Router>
        </div>
      )
  }
}

export default App;