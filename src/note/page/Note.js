import React from 'react'
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { deleteMarkDown, getCurrentId } from "../actions/noteAction";
import { getAllNotes } from "../../home/actions/homeAction";

class Note extends React.Component{
  constructor(props){
    super(props);
    this.props.getCurrentId(this.props.match.params.id);
    this.props.getAllNotes();
  }
  deleteMarkDown(event){
    this.props.deleteMarkDown(event.target.id);
    window.location.href = '/';
  }
  handlerBackHome(){
    window.location.href = '/';
  }
  render() {
    const result = this.props.markDowns;
    const currentId = this.props.currentId;
    return(
      <div>
        {
          result.map((value) => {
            if (Number.parseInt(value.id) === Number.parseInt(currentId)) {
              return (
                <div>
                  <h1>{value.title}</h1>
                  <hr/>
                  <p>{value.description}</p>
                  <button onClick={this.deleteMarkDown.bind(this)} id={value.id}>删除</button>
                  <button onClick={this.handlerBackHome.bind(this)} >取消</button>
                </div>
              )
            }
          })
        }
      </div>
    )
  }
}
const mapStateToProps = state => ({
  currentId: state.note.currentId,
  markDowns: state.notes.markDowns,
  result:state.note.result
});

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    getCurrentId,
    getAllNotes,
    deleteMarkDown
  }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Note);
