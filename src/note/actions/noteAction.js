export const getCurrentId = (currentId) => (dispatch) => {
  dispatch({
    type: 'GET_CURRENT_ID',
    currentId: currentId
  });
};

export const deleteMarkDown = (id) => (dispatch) => {
  fetch('http://localhost:8080/api/posts/'+id,{
    method:"DELETE",
  }
)
    .then(response =>response.json())
    .then(result => {
      dispatch({
        type:'DELETE_MARKDOWN_BY_ID',
        result:result
      })
    })
};

