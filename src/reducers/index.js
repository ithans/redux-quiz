import {combineReducers} from "redux";
import homeReducer from "../home/reducer/homeReducer";
import noteReducer from "../note/reducer/noteReducer";
import createReducer from "../create/reducer/createReducer";

const reducers = combineReducers({
  notes:homeReducer,
  note:noteReducer,
  creater:createReducer
});
export default reducers;